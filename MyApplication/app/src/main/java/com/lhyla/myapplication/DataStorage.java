package com.lhyla.myapplication;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.R.id.content;
import static android.R.id.message;

public class DataStorage extends AppCompatActivity {

    private static final String TAG = "LH";

    @BindView(R.id.data_storage_act_phone_edit_text)
    protected EditText phoneEditText;

    LinearLayoutManager linearLayoutManager;
    List<String> phoneList;
    DataStorageAdapter dataStorageAdapter;
    RecyclerView recyclerView;
    String filename;

    DividerItemDecoration dividerItemDecoration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_storage);
        ButterKnife.bind(this);

        filename = "myfile";
        phoneList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.data_storage_act_recycler_view);

        linearLayoutManager = new LinearLayoutManager(this);

        recyclerView.setLayoutManager(linearLayoutManager);
    }

    @OnClick(R.id.data_storage_act_save_to_file_btn)
    protected void saveToFileBtnOnClick() {

        String message = phoneEditText.getText().toString() + "\n";
        try {
            FileOutputStream fileOutputStream;
            fileOutputStream = openFileOutput(filename, Context.MODE_APPEND);
            fileOutputStream.write(message.getBytes());
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.data_storage_clear_data_btn)
    protected void clearDataBtnOnClick() {
        phoneList.clear();
        String message = "";
        try {
            FileOutputStream fileOutputStream;
            fileOutputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            fileOutputStream.write(message.getBytes());
            fileOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        loadFromFileBtnOnClick();

    }

    @OnClick(R.id.data_storage_act_load_from_file_btn)
    protected void loadFromFileBtnOnClick() {
        FileInputStream fileInputStream = null;

        try {
            recyclerView.removeItemDecoration(dividerItemDecoration);
            phoneList.clear();
            fileInputStream = getApplicationContext().openFileInput("myfile");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            String line = "";
            StringBuilder stringBuilder = new StringBuilder();
            while ((line = bufferedReader.readLine()) != null) {
                phoneList.add(line);
            }
            String s = phoneList.toString();
            Log.d(TAG, s);
            dataStorageAdapter = new DataStorageAdapter(phoneList, getApplicationContext());

            dividerItemDecoration = new DividerItemDecoration(
                    recyclerView.getContext(),
                    linearLayoutManager.getOrientation());

            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.setAdapter(dataStorageAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
