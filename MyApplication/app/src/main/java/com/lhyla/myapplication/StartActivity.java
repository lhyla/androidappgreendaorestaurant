package com.lhyla.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.start_act_main_act_bnt)
    protected void mainActBtnOnClick() {
        Intent intent = new Intent(this, RestaurantActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.start_act_data_storage)
    protected void dataStorageBtnOnClick() {
        Intent intent = new Intent(this, DataStorage.class);
        startActivity(intent);
    }
}
