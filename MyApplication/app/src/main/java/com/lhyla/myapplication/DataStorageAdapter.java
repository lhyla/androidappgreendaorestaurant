package com.lhyla.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by RENT on 2017-07-14.
 */

public class DataStorageAdapter extends RecyclerView.Adapter<DataStorageAdapter.MyViewHolder> {
    List<String> phoneList;
    Context context;


    public DataStorageAdapter(List<String> phoneList, Context context) {
        this.phoneList = phoneList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.activity_data_storage_row,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final String item = phoneList.get(position);
        holder.phoneNum.setText(item);
    }

    @Override
    public int getItemCount() {
        return phoneList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView phoneNum;

        public MyViewHolder(View itemView) {
            super(itemView);
            phoneNum = (TextView) itemView.findViewById(R.id.row_data_storage_phone);
        }
    }
}
