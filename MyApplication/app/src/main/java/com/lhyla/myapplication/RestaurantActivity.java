package com.lhyla.myapplication;

import android.content.Context;
import android.nfc.Tag;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.EditText;

import com.google.gson.Gson;
import com.lhyla.myapplication.db.DaoMaster;
import com.lhyla.myapplication.db.DaoSession;
import com.lhyla.myapplication.db.Product;
import com.lhyla.myapplication.db.ProductDao;

import org.greenrobot.greendao.database.Database;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RestaurantActivity extends AppCompatActivity {

    @BindView(R.id.restaurant_act_name_edit_text)
    protected EditText nameEditText;

    @BindView(R.id.restaurant_act_price_edit_text)
    protected EditText priceEditText;

    public DaoSession daoSession;
    public ProductDao productDao;
    public List<Product> productList;
    public RecyclerView recyclerView;
    public RestaurantAdapter restaurantAdapter;

    public String filename;

    private static final String TAG = "LH";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);
        ButterKnife.bind(this);

        filename = "cart_file.sda";

        productList = new ArrayList<>();

        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(this, "users-db");
        Database db = devOpenHelper.getWritableDb();
        DaoMaster daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        productDao = daoSession.getProductDao();

        recyclerView = (RecyclerView) findViewById(R.id.restaurant_act_recycle_view);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);

        addItemsToRecycleView();

    }

    @OnClick(R.id.restaurant_act_add_btn)
    protected void addBtnOnClick() {
        Product product = createNewProduct();

        if (product != null) {
            productDao.insert(product);
            addItemsToRecycleView();
        }
    }


    public Product createNewProduct() {
        boolean priceIsEmpty = priceEditText.getText().toString().isEmpty();
        boolean nameIsEmpty = nameEditText.getText().toString().isEmpty();

        if (!(priceIsEmpty || nameIsEmpty)) {
            float price = Float.parseFloat(priceEditText.getText().toString());
            String name = nameEditText.getText().toString();
            Product product = new Product();
            product.setName(name);
            product.setPrice(price);
            return product;
        } else {
            return null;
        }
    }

    @OnClick(R.id.restaurant_act_load_bnt)
    protected void loadDataBtnOnClick() {
        addItemsToRecycleView();
    }

    private void addItemsToRecycleView() {
        productList = productDao.queryBuilder().list();
        restaurantAdapter = new RestaurantAdapter(productList, getApplicationContext());
        recyclerView.setAdapter(restaurantAdapter);
    }

    @OnClick(R.id.restaurant_act_delete_btn)
    protected void deleteDataBtnOnClick() {
        productDao.deleteAll();
        addItemsToRecycleView();
    }

    @OnClick(R.id.restaurant_act_safe_to_external_storage_btn)
    protected void saveExternalStorageBtnOnClick() {
        saveExternalStorage();
    }

    protected void saveExternalStorage() {

        Log.d(TAG, "saveExternalStorage()");
        //String folder = Environment.getExternalStorageDirectory().toString();
        File file = new File(getApplicationContext().getFilesDir(), filename);
        Product product = new Product();

        product.setPrice(25f);
        product.setName("MY PRODUCT");

        Product product1 = new Product();
        product1.setPrice(10f);
        product1.setName("SECOND PRODUCT");
        productDao.insert(product);
        productDao.insert(product1);

        try {
            Log.d(TAG, "saveExternalStorage() try");

            FileOutputStream stream = new FileOutputStream(file);
            productList = productDao.queryBuilder().list();


            Gson gson = new Gson();
            String jsonOrders = gson.toJson(productList);
            stream.write(jsonOrders.getBytes());

        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "saveExternalStorage() Exception");
        }
    }

    @OnClick(R.id.restaurant_act_load_from_external_storage_btn)
    protected void loadFromExternalStorageBtnOnClick() {
        loadExternalStorage();
    }

    protected void loadExternalStorage() {
        try {
            productList.clear();
            FileInputStream fileInputStream = getApplicationContext().openFileInput(filename);
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder stringBuilder = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }

            Gson gson = new Gson();
            Product[] products = gson.fromJson(stringBuilder.toString(), Product[].class);

            List<Product> listOfProducts = Arrays.asList(products);

            productDao.deleteAll();

            for (Product product : listOfProducts) {
                productDao.insert(product);
            }

            productList = productDao.loadAll();
            addItemsToRecycleView();

            Log.d(TAG, stringBuilder.toString());

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}